require "test_helper"

class TasksControllerTest < ActionDispatch::IntegrationTest
  TOKEN = ApplicationController::TOKEN
  setup do
    @task = tasks(:one)
  end

  test "should get index" do
    get tasks_url, headers: { 'HTTP_AUTHORIZATION' => TOKEN }, as: :json
    assert_response :success
  end

  test 'should get tasks for employee' do
    employee_one = Employee.first
    get tasks_url(employee: employee_one), headers: { 'HTTP_AUTHORIZATION' => TOKEN }, as: :json
    assert_response :success
    assert_equal employee_one.tasks.count, JSON(response.body).count
  end

  test 'should get future tasks for employee' do
    employee_one = Employee.includes(:tasks).first
    task_count = employee_one.tasks.where('tasks.execution_date > ?', Time.now).count
    get tasks_url(employee: employee_one, future_tasks: "true"), headers: { 'HTTP_AUTHORIZATION' => TOKEN }, as: :json
    assert_response :success
    assert_equal task_count, JSON(response.body).count
  end


  test 'should return record not found' do
    get tasks_url(employee: 1), headers: { 'HTTP_AUTHORIZATION' => TOKEN }, as: :json
    assert_response 404
  end

  test 'should fail authentication' do
    get tasks_url, as: :json
    assert_response 401
  end
end
