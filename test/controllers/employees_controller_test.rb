# frozen_string_literal: true

require 'test_helper'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  TOKEN = ApplicationController::TOKEN
  setup do
    @employee = employees(:one)
  end

  test 'should get index' do
    get employees_url, headers: { 'HTTP_AUTHORIZATION' => TOKEN }, as: :json
    assert_response :success
  end
  test 'should get active employees' do
    get employees_url(status: :active),  headers: { 'HTTP_AUTHORIZATION' => TOKEN }, as: :json
    assert_response :success
    assert_equal ["active"], JSON(response.body).map {|r| r["status"]}.uniq
  end


  test 'should fail authentication' do
    get employees_url, as: :json
    assert_response 401
  end
end
