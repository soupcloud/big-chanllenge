class TasksController < ApplicationController

  # GET /tasks
  def index
    @tasks = Task.all
    @tasks = Employee.includes(:tasks).find(params[:employee]).tasks if params.include?(:employee)
    @tasks = @tasks.where('tasks.execution_date > ?', Time.now) if params.include?('future_tasks') && params[:future_tasks] == "true"

    render json: @tasks.limit(20), each_serializer: TasksSerializer
  end

end
