class EmployeesController < ApplicationController

  # GET /employees
  def index
    @employees = Employee.all
    @employees = @employees.where(status: params[:status]) if params.include? :status
    render json: @employees.limit(20), each_serializer: EmployeeSerializer
  end
end
