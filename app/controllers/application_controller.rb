# frozen_string_literal: true

class ApplicationController < ActionController::API
  before_action :authenticate
  rescue_from ActiveRecord::RecordNotFound, with: :handle_not_found

  TOKEN = 'secret'

  private

  def authenticate
    authenticate_user_with_token || handle_bad_authentication
  end

  def authenticate_user_with_token
    request.headers["HTTP_AUTHORIZATION"] == TOKEN
  end

  def handle_bad_authentication
    render json: { message: 'Bad credentials' }, status: :unauthorized
  end

  def handle_not_found
    render json: { message: 'Record not found' }, status: :not_found
  end
end
