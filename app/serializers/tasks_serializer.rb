class TasksSerializer < ActiveModel::Serializer
  attributes :id ,:name, :execution_date
  # has_many :employees, serializer: EmployeeSerializer



  def execution_date
    object.execution_date.to_formatted_s(:db)
  end
end
