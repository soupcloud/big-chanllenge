class EmployeeSerializer < ActiveModel::Serializer
  attributes :id, :name, :email, :status
  # has_many :tasks, serializer: TasksSerializer
end
