# README
### Getting started
```bash
rvm use 2.7.2
gem  install bundler
bundle install 
rails db:migrate
rails db:fixtures:load 
rails s
```


#Apis
###Employee 
Return a list of maximum 20 result of employees
```ruby
get /employees.json
```
#### Parmaters
| Parameters | Description                                                |
|------------|------------------------------------------------------------|
| status     | options: [:training`, :active] filters employees by status |  

####Example

```bash
curl -H "Authorization: secret" http://localhost:3000/employees.json 
   {
      "email" : "MyString",
      "id" : 298486374,
      "name" : "MyString",
      "status" : "active"
   },
   {
      "email" : "MyString",
      "id" : 980190962,
      "name" : "MyString",
      "status" : "training"
   }
]
```
###Tasks

```ruby
get /tasks.json
```
Return a list of maximum 20 result of task

| Parameters   | Description                                                        |
|--------------|--------------------------------------------------------------------|
| employee     | id of employee. Returns tasks for empolyee                         | 
| future_tasks | if true will return future date tasks or will retun all dated task |  
###Example
```bash
curl -H "Authorization: secret" http://localhost:3000/tasks.json 
[
   {
      "execution_date" : "2022-03-24 01:31:58",
      "id" : 113629430,
      "name" : "user one future task"
   },
   {
      "execution_date" : "2022-03-24 01:31:58",
      "id" : 281110143,
      "name" : "user two future task "
   },
   {
      "execution_date" : "2022-03-20 01:31:58",
      "id" : 298486374,
      "name" : "user two current task"
   },
   {
      "execution_date" : "2022-03-20 01:31:58",
      "id" : 980190962,
      "name" : "user one current task"
   }
]
```
